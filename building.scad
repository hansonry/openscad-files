/************************************
 * Building Generator
 * By: Ryan Hanson
 * Date: 01/15/2020
 *
 * Creates a building from provided string array.
 *
 * Make it as big as your computer can handle :)
 * Each wall section is a unit cube in size.
 * Scale for your pleasure.
 ************************************/
// " " = Nothing
// "#" = Wall
// "w" = Window
// "d" = Door
data = [
   "    ##w##",
   "    #   #",
   "    #   #",
   "##w###d##",
   "#       #",
   "d       w",
   "#       #",
   "##w###w##"
];

building(data);

module building(data, wall_thickness=0.1, center=false)
{
   function isWall(c) = c == "#" || c == "w" || c == "d";
   // find max width of data rows
   width = max([ for(row=data) len(row) ]);
   height = len(data);
   // Convert to one large array while making sure all the rows are 
   // the same width.
   xdata = [ 
      for(y=[0:height-1])
         let (w=len(data[y]))
         for(x=[0:width-1]) 
            (x < w) ? 
               [ x, y, data[y][x] ] : 
               [ x, y, " "        ]  
   ];
   // xdata should now be an array of [x, y, character]
         
   function isWallAt(x, y) = 
      (x >= 0 && y >= 0 && x < width && y < height) ?       
         let(index = x + y * width)
         isWall(xdata[index][2]) :
         false;
         
   trans = center ? 
         [-width / 2, -height / 2, -0.5] :
         [0, 0, 0]; 

   translate(trans) union() for(pos=xdata)
   {
      x = pos[0];
      y = pos[1];
      c = pos[2];
      offset = [x, y, 0];
      
      neighbors_xy = [ 
         [x + 1, y    ], 
         [x,     y + 1], 
         [x - 1, y    ], 
         [x,     y - 1] 
      ];
      neighbors_flags = [ 
         for(n=neighbors_xy) isWallAt(n[0], n[1])
      ];
      
      if(c == "#")
      {
         translate(offset)
            wall(neighbors_flags, wall_thickness, []);
      }
      else if(c == "d")
      {
         translate(offset)
            wall(neighbors_flags, wall_thickness, ["door"]);
      }
      else if(c == "w")
      {
         translate(offset)
            wall(neighbors_flags, wall_thickness, ["window"]);
      }
   }
}

module wall(sections, thickness=0.1, decorations=[])
{
   section_length = (1 - thickness) / 2;
   data = [
      [section_length + thickness, section_length,             section_length, thickness],   
      [section_length,             section_length + thickness, thickness,      section_length],
      [0,                          section_length,             section_length, thickness],   
      [section_length,             0,                          thickness,      section_length],
   ];
   
   difference()
   {
      union()
      {
         translate([section_length, section_length, 0])
            cube([thickness, thickness, 1]);
      
         for(index=[0:len(sections)])
            if(sections[index])
               translate([data[index][0], data[index][1], 0])
                  cube([data[index][2], data[index][3], 1]);
      }
      for(decoration=decorations)
      {
         if(decoration == "door")
         {
            translate([0.3, 0.3, 0])
               cube([0.4, 0.4, 0.8]);
         }
         else if(decoration == "window")
         {
            translate([0.2, 0.2, 0.3])
               cube([0.6, 0.6, 0.5]);
         }
      }
   } 
}
